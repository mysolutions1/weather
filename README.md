## Laravel Weather API 

Hello, below is simple but flexible Laravel API. 

| NOTICE: I did not catch all the errors here, mostly METHOD errors! |


Base Url: https://weather-api-seconsoft.com/api

<br /> 

## Below is the various endpoints

<b>POST:</b> /register

```javascript

Params: {
	name: 'required',
	email: 'required',
	password: 'required'
}

```

<b>Description:</b> This API will create authorized users profile with an associated personalized token. The Token will enable you access other endpoints

<br /><br />

<b>POST:</b> /login

```javascript

Params: {
	email: 'required',
	password: 'required'
}

```

<b>Description:</b> This should login and return your personalized token


<br /><br />

<b>POST:</b> /gateway

```javascript

Params: {
	tag: 'required|string|unique - Just an Id for the new gateway',
	url: 'required - [example: https://api.openweathermap.org/data/2.5/weather]'
	key: 'required|string [The API Key for the Weather API_URL you entered]'
}

```

<b>Description:</b> stores the API gateway to be access by the [tag] name <i>(authentication required)</i>

<br /><br />

<b>GET:</b> /report

```javascript

Params: {
	tag: 'required|string [Get the API you want to use based on the tag name]',
	q|state|lat|log: 'The city | longitude or latitude'
}

```

<b>Description:</b> Get the weather report <i>(authentication required)</i>

<br /><br />

<b>GET:</b> /

<b>Description:</b> Fetch all the gateway the authenticated user entered <i>(authentication required)</i>

<br /><br />

<b>PUT:</b> /edit/{id}

```javascript

Params: {
	tag: 'string',
	key: 'string ['
}

```

<b>Description:</b> updates tag or key values in gateway credentials. <i>(authentication required)</i>

<br /><br />
<b>DELETE:</b> /delete/{id}

<b>Description:</b> deletes the gateway data using its id <i>(authentication required)</i>



<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeatherGateWay extends Model
{
    use HasFactory;

    protected $table = "weather_gate_ways";
    
    protected $fillable = [
        "user_id",
        "tag",
        "api_url",
        "log_lat",
        "api_key"
    ];
}

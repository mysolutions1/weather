<?php

namespace App\Http\Controllers;


use App\Http\Controllers\WeatherSource;
use App\Models\WeatherGateWay;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Laravel\Sanctum\PersonalAccessToken;

class WeatherController extends WeatherSource
{
    /**
     * The function takes a request object, makes a call to the source API, gets the API URL and API
     * key, then makes a call to the source API with the request object and the API key
     * 
     * @param Request request This is the request object that is passed to the controller.
     * 
     * @return The response from the API is being returned.
     */
    public function report(Request $request){
        $response = json_encode($this->source_api($request));
        $response_json = json_decode($response, true);
        
        if(!is_null($response_json)){
            $endpoint = $response_json["api_url"];
            $api_key = $response_json["api_key"];

            $client = new \GuzzleHttp\Client();
            
            /* Merging the request object with the API key. */
            $new_request = array_merge($request->all(), [
                'key' => $api_key, 
                "appId" => $api_key, 
                "id" => $api_key, 
                "appid" => $api_key, 
                "app-id" => $api_key, 
                "api" => $api_key
            ]);
            
            try{
                $response = $client->request('GET', $endpoint, ['query' => $new_request]);
                return $response->getBody();
            } catch(\Exception $e){
                echo "Oops! An error occured, Kindly check details below \n\n\r " . $e->getMessage();
            }   
        }
    }

    /**
     * It takes a bearer token, finds the user id associated with the token, and then returns the
     * weather source associated with the tag
     * 
     * @param Request request The request object.
     * 
     * @return The response is a JSON object that contains the weather data for the location that was
     * requested.
     */
    protected function source_api(Request $request){

        $plainToken = $request->bearerToken();    

       /* Finding the token that is associated with the bearer token. */
        $token_root = PersonalAccessToken::findToken($plainToken);

        $user_id = $token_root->tokenable->id;

        $field = $request->validate([
            "tag" => "required",
        ]);

        /* Finding the weather gateway of the user. */
        $response = WeatherGateWay::where([
            ["tag", '=', $field["tag"]],
            ["user_id", "=", $user_id]
        ])->first();

        return $response;
    }
    
    /**
     * It stores the weather gateway of the user.
     * 
     * @param Request request The request object
     * 
     * @return The gateway of the user.
     */
    protected function gateway(Request $request){
        return $this->store($request, $request->bearerToken());
    }
}

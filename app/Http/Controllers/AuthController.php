<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    /**
     * It creates a new user, creates a token for that user, and returns the user and token in a
     * response
     * 
     * @param Request request This is the request object that contains the data that was sent to the
     * API.
     * 
     * @return The user and the token
     */

    public function register(Request $request){
        $fields = $request->validate([
            "name" => "required|string",
            "email" => "required|string|unique:users,email",
            "password" => "required|string|min:6"
        ]);

        $user= User::create([
            "name" => $fields["name"],
            "email" => $fields["email"],
            "password" => bcrypt($fields["password"])
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;
        
        $response = [
            "user" => $user,
            "token" => $token
        ];

        return response($response, 200);
    }

    /**
     * If the user's email and password are not found in the database, return a 401 error. Otherwise,
     * create a token for the user and return it
     * 
     * @param Request request This is the request object that contains the email and password.
     * 
     * @return A JSON response with the access token and token type.
     */
    public function login(Request $request){
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                'message' => 'Invalid login details'
            ], 401);
        }
            
        $user = User::where('email', $request['email'])->firstOrFail();
        
        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer'
        ]);
    }
}

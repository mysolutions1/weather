<?php

namespace App\Http\Controllers;

use App\Models\WeatherGateWay;

use Illuminate\Http\Request;
use Laravel\Sanctum\PersonalAccessToken;

class WeatherSource extends Controller 
{
    protected $user_id = 0;
    
    private function get_user_id(Request $request){

        if($this->user_id <= 0){
            $plainToken = $request->bearerToken();      
            $token_root = PersonalAccessToken::findToken($plainToken);
            $this->user_id = $token_root->tokenable->id; 
        } 

        return $this->user_id;
    }

    /**
     * It returns all the weather gateways
     * 
     * @return All the data in the WeatherGateWay table.
     */
    public function index(Request $request)
    {

        $user_id = $this->get_user_id($request);

        return WeatherGateWay::where("user_id", "=", $user_id)->get(["id", "tag", "api_url"]);
    }
    
    /**
     * It creates a new record in the database
     * 
     * @param Request request The request object.
     * @param type The type of the API you want to add.
     * 
     * @return the created object.
     */
    public function store(Request $request)
    {

        $user_id = $this->get_user_id($request);

        $request->validate([
            "url" => "required",
            "key" => "required"
        ]);

        return WeatherGateWay::create([
            "user_id" =>  $user_id,
            "tag" => $request->get("tag"),
            "api_url" => $request->get("url"),
            "api_key" => $request->get("key")
        ]);
    }
    
    /**
     * > It updates the weather gateway with the new tag and api key
     * 
     * @param Request request The request object.
     * @param id The id of the gateway you want to update.
     * 
     * @return The updated gateway.
     */
    public function update(Request $request, $id)
    {

        if(!empty($id)){
            $gateway = WeatherGateWay::find($id);
            $gateway_json = json_decode($gateway, true);
           
            if(!is_null($gateway))
                $gateway->update([
                    "tag" => $request->get("tag", $gateway_json["tag"]),
                    "api_key" => $request->get("key", $gateway_json["api_key"])
                ]);
            return $gateway;
        }
    }

   /* Deleting the record from the database. */
    public function destroy($id)
    {
        return WeatherGateWay::destroy($id);
    }
}

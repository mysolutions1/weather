<?php

use App\Http\Controllers\WeatherController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
*/

Route::post("/register", [AuthController::class, "register"]);
Route::post("/login", [AuthController::class, "login"]);

Route::group(["middleware" => ["auth:sanctum"]], function(){
    /** This is a route that is being defined. The first parameter is the URL that will be used to
    *   access the route. The second parameter is an array that contains the controller and the method
    *   that will be used to handle the request. 
    */
    Route::get("/", [WeatherController::class, "index"]);
    Route::get("/report", [WeatherController::class, "report"]);
    Route::post("/gateway", [WeatherController::class, "gateway"]);
    Route::put("/edit/{id}", [WeatherController::class, "update"]);
    Route::delete("/delete/{id}", [WeatherController::class, "destroy"]);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

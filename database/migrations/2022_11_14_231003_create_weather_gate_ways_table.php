<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherGateWaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_gate_ways', function (Blueprint $table) {
            $table->id();
            $table->integer("user_id", 255);
            $table->string("tag", 100);
            $table->string("api_url", 1000);
            $table->boolean("log_lat")->default(false);
            $table->string("api_key", 1000);
            $table->tinyInteger("deleted", false)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_gate_ways');
    }
}
